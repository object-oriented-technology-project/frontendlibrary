import React, {Component} from "react";
import classes from './Toolbar.module.css'
import NavigationItems from "./NavigationItems/NavigationItems"
import UserLogin from "../UserLogin/UserLogin";
import {connect} from "react-redux";
import * as constants from "../../util/Constants";
import Button from "react-bootstrap/Button";
import * as actionCreator from "../../store/actions/index"

class Toolbar extends Component {
    render() {
        let option = <UserLogin/>;

        if(this.props.role === constants.ROLE_CONSUMER || this.props.role === constants.ROLE_LENDER){
            option = (
                <React.Fragment>
                    <h3>Hola {this.props.name}!</h3>
                    <Button variant="dark" onClick={this.props.onLogout}>CERRAR SESION</Button>
                </React.Fragment>
            );
        } else {

        }

    return(
        <header className={classes.Toolbar}>
            <nav className={classes.DesktopOnly}>
                <NavigationItems role={this.props.role}/>
            </nav>
            {option}
        </header>
    )};
}


const mapStateToProps = state => {
    return{
        role: state.auth.role,
        name: state.auth.name
    };
};

const mapDispatchToProps = dispatch => {
    return{
        onLogout: () => dispatch(actionCreator.onLogout())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);