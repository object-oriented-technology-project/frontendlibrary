import React from "react";
import NavigationItem from "./NavigationItem/NavigationItem";
import classes from "./NavigatioItems.module.css"
import * as constants from "../../../util/Constants";

const navigationItems = (props) => {
    let navItems = null;

    if(props.role === constants.ROLE_CONSUMER || props.role === constants.ROLE_LENDER) {
        navItems = (
            <React.Fragment>
                <NavigationItem link={constants.ROUTE_PRESTAMOS}>Prestamos</NavigationItem>
                <NavigationItem link={constants.ROUTE_USUARIO_DATOS}>Datos Usuario</NavigationItem>
            </React.Fragment>
        )
    }

    return (
    <ul className={classes.NavigationItems}>
        <NavigationItem link="/">Home</NavigationItem>
        {navItems}
    </ul>
    );
};

export default navigationItems;