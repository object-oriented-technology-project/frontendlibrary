import React from "react";
import classes from "./Input.module.css"

const input = (props) =>{
    let inputElement = null;
    const inputClasses = [classes.InputElement];

    if(!props.valid && props.shouldValidate && props.touched){
        inputClasses.push(classes.Input);
    }

    switch (props.elementType) {
        case('input'):
            inputElement = <input
                className={inputClasses.join(' ')}
                value={props.value}
                onChange={props.onChange}
                {...props.elementConfig} />; break;

        case('textarea'):
            inputElement=<textarea
                className={inputClasses.join(' ')}
                value={props.value}
                onChange={props.onChange}
                {...props.elementConfig}/>; break;

        case('select'):
            inputElement = <select
                    className={inputClasses.join(' ')}
                    value={props.value}
                    onChange={props.onChange}>
                    {props.elementConfig.options.map(option =>(
                        <option key={option.value} value={option.value}>{option.displayValue}</option>
                    ))}
                </select>;
                break;
        default: //Same as input case
            inputElement = <input
                className={inputClasses.join(' ')}
                value={props.value}
                onChange={props.onChange}
                {...props.elementConfig} />;
        }

    return(
        <div className={classes.Input}>
            <label className={classes.Label}>{props.label}</label>
            {inputElement}
        </div>
    );
    };

export default input;