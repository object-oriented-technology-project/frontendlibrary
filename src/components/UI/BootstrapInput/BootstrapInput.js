import React from "react";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import * as Util from "../../../util/Utility"

const bootstrapInput = (props) => {
    let inputElement = null;
    let errorMessages = [];
    let errors = null;

    //EMPIEZA VALIDACIONES
    if(!props.config.disabled){
        if(props.shouldValidate.required && props.touched){
            if(!Util.requiredValidation(props.value)){
                errorMessages.push("Campo Obligatorio");
            }
        }

        if(props.shouldValidate.isEmail && props.touched){
            if(!Util.emailValidation(props.value)){
                errorMessages.push("Email Incorrecto");
            }
        }

        if(props.shouldValidate.maxLength && props.touched){
            if(!Util.maxLengthValidation(props.value, props.shouldValidate.maxLength)){
                errorMessages.push("Tamaño maximo: " +props.shouldValidate.maxLength);
            }
        }

        if(props.shouldValidate.minLength && props.touched){
            if(!Util.minLengthValidation(props.value, props.shouldValidate.minLength)){
                errorMessages.push("Tamaño minimo: " +props.shouldValidate.minLength);
            }
        }

        if((props.shouldValidate.greaterThan!==undefined && props.shouldValidate.greaterThan !== null) && props.touched){
            const value = parseInt(props.value);
            const greaterThan = props.shouldValidate.greaterThan;
            if(!(value > greaterThan)){
                errorMessages.push("Valor debe ser mayor a " +greaterThan);
            }
        }

        if((props.shouldValidate.lesserThan!==undefined && props.shouldValidate.lesserThan !== null) && props.touched){
            const value = parseInt(props.value);
            const lesserThan = props.shouldValidate.lesserThan;
            if(!(value < lesserThan)){
                errorMessages.push("Valor debe ser menor a " +lesserThan);
            }
        }

        if(props.shouldValidate.passwordCheck && props.touched) {
            if (props.passwordCheck !== props.value) {
                errorMessages.push("Contraseña diferente");
            }
        }

        if(props.shouldValidate.isImage){
            console.log(props.config.imageFile);
            if(props.config.imageFile !== null){
                if(props.config.imageFile.type !== "image/jpeg") {
                    errorMessages.push("Archivo no valido");
                }
            }
        }

        if(errorMessages.length>0){
            errors =(
                <Alert variant="danger">
                    {errorMessages.map(message => (
                        <React.Fragment>
                            <p className="mb-0">- {message}</p>
                        </React.Fragment>
                    ))}
                </Alert>
            );
        }
    }
    //TERMINA VALIDACIONES


    switch (props.elementType) {
        case('input'):
            inputElement = (
                <Form.Group key={props.key}>
                    {errors}
                    <Form.Label><b>{props.label}</b></Form.Label>
                    <Form.Control
                        value={props.value}
                        {...props.config}
                        onChange={(event) => onChange(props, event)}/>
                </Form.Group>
            );
            break;
        case('textarea'):
            inputElement = (
                <Form.Group key={props.key}>
                    {errors}
                    <Form.Label><b>{props.label}</b></Form.Label>
                    <Form.Control
                        as="textarea"
                        value={props.value}
                        {...props.config}
                        onChange={(event) => onChange(props, event)}/>
                </Form.Group>
            );
            break;
        case('select'):
            inputElement = (
                <Form.Group key={props.key}>
                    {errors}
                    <Form.Label><b>{props.label}</b></Form.Label>
                    <Form.Control
                        value={props.value}
                        {...props.config}
                        onChange={(event) => onChange(props, event)}
                        as="select">
                        {props.options.map(option =>{
                            return <option value={option.value}>{option.label}</option>
                        })}
                    </Form.Control>
                </Form.Group>
            );
            break;
        case('file'):
            inputElement = (
                <Form.Group key={props.key}>
                    {errors}
                    <Form.Label><b>{props.label}</b></Form.Label>
                    <div className="mb-3">
                        <Form.File id="formcheck-api-custom" custom>
                            <Form.File.Input onChange={(event) => onChange(props, event)} isValid />
                            <Form.File.Label data-browse="Button text">
                                Custom file input
                            </Form.File.Label>
                            {(props.fileAdded && props.valid) ? <Form.Control.Feedback type="valid">You did it!</Form.Control.Feedback> : null}
                            {(props.fileAdded && !props.valid) ? <Form.Control.Feedback type="danger">WRONG FILE!</Form.Control.Feedback> : null}
                        </Form.File>
                    </div>
                </Form.Group>
            );
            break;
    }

    return(inputElement);
};

const onChange = (props, event) =>{
    let isValid = true;
    if(!props.config.disabled){
        if(props.shouldValidate.required && props.touched){
            if(!Util.requiredValidation(event.target.value)){
                isValid = false;
            }
        }

        if(props.shouldValidate.isEmail && props.touched){
            if(!Util.emailValidation(event.target.value)){
                isValid = false;
            }
        }

        if(props.shouldValidate.maxLength && props.touched){
            if(!Util.maxLengthValidation(event.target.value, props.shouldValidate.maxLength)){
                isValid = false;
            }
        }

        if(props.shouldValidate.minLength && props.touched){
            if(!Util.minLengthValidation(event.target.value, props.shouldValidate.minLength)){
                isValid = false;
            }
        }

        if(props.shouldValidate.passwordCheck && props.touched){
            if(props.passwordCheck !== event.target.value){
                isValid = false;
            }
        }

        if(props.shouldValidate.isImage){
            if(event.target.files[0] !== null){
                if(event.target.files[0].type !== "image/jpeg") {
                    isValid=false;
                }
            }
        }
    } else {
        isValid = false;
    }

    if(props.elementType === 'file'){
        props.onChangeFile(props.id, event.target.files[0], isValid)
    } else {
        props.onChange(props.id, event, isValid);
    }
};

export default bootstrapInput;