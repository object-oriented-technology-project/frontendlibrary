import React, {Component} from "react";
import Input from "../UI/Input/Input"
import Button from "react-bootstrap/Button";
import * as Util from "../../util/Utility"
import * as constants from "../../util/Constants";
import {connect} from "react-redux";
import classes from "./UserLogin.module.css"
import * as actionCreators from "../../store/actions/index"
import {withRouter} from "react-router";

class UserLogin extends Component {
    state = {
        controls: {
            username: {
                value: "",
                label: "Nombre de Usuario",
                elementType: "input",
                elementConfig: {
                    type: "text"
                }, validation: {
                    required: true,
                    maxLength: 255
                },
                valid: false
            },
            password: {
                label: "Contraseña",
                value: "",
                elementType: "input",
                elementConfig: {
                    type: "password"
                }, validation: {
                    required: true,
                    maxLength: 255
                },
                valid: false,
                touched: false
            },
        }
    };

    onInputChangeHandler = (event, controlName) => {
        const updatedControl = Util.updateObject(
            this.state.controls[controlName],
            {
                value: event.target.value,
                valid: Util.checkValidity(event.target, this.state.controls[controlName].validation),
                touched: true
            }
        );

        const updatedControls = Util.updateObject(
            this.state.controls,
            {[controlName]:updatedControl}
        );

        this.setState(
            {controls: updatedControls}
        );
    };

    onSubmitHandler = () =>{
        const username = this.state.controls.username.value;
        const password = this.state.controls.password.value;
        this.props.onLogin(username, password);
    };

    onUserRegisterHandler = () =>{
        this.props.history.push(constants.ROUTE_USUARIO_REGISTER);
    };

        render() {
            const formElementsArray = [];

            for (let key in this.state.controls) {
                formElementsArray.push(
                    {
                        id: key,
                        config: this.state.controls[key]
                    }
                );
            }

            let form = formElementsArray.map(formElement => (
                <Input
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    label={formElement.config.label}
                    valid={formElement.config.valid}
                    shouldValidate={formElement.config.validation}
                    onChange={(event) => this.onInputChangeHandler(event, formElement.id)}/>
            ));

            return (
                <div className={classes.UserLogin}>
                    <form onSubmit={this.onSubmitHandler}>
                        {form}
                        <Button onClick={this.onSubmitHandler} variant="primary">Log in</Button >
                    </form>
                    <Button variant="info" onClick={this.onUserRegisterHandler}>Registrarse</Button >
                </div>
            );
        }
    }

    const mapDispatchToProps = dispatch => {
        return {
            onLogin: (username, password) => dispatch(actionCreators.auth(username, password))
        }
    };


    export default withRouter(connect(null, mapDispatchToProps)(UserLogin));
