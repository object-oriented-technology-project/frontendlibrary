import React from "react";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import * as Util from "../../../util/Utility";
import ReactStars from 'react-rating-stars-component'
import * as constants from "../../../util/Constants";

const cardDevuelto = (props) =>{
    return (
        <Card>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                    Prestamos Devueltos
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="3">
                <Card.Body>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Libro</th>
                            <th>Dias Prestados</th>
                            <th>Precio Dia</th>
                            <th>Consumidor</th>
                            <th>Fecha Prestamo</th>
                            <th>{props.role===constants.ROLE_LENDER ? 'Calificar Consumidor' : 'Calificar Libro'}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.lends.map(lend => {
                            return(
                                <tr key={lend.id}>
                                    <td>{lend.libro}</td>
                                    <td>{lend.cantidadDias}</td>
                                    <td>{lend.precioDia}</td>
                                    <td>{lend.consumidorUsername}</td>
                                    <td>{Util.getFormattedDate(lend.fechaPrestamo)}</td>
                                    <td>
                                        {getReactStars(lend, props.role, props)}
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </Table>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
};

const getReactStars = (lend, role, props) =>{
    let reactStars = null;

    if(role===constants.ROLE_LENDER){
        reactStars = (
            <ReactStars
            count={5}
            size={24}
            value={lend.calificacionConsumer}
            onChange={(rating)=>props.changeStateHandler(lend.id, constants.LEND_STATE_CALIFICADO, {ratingConsumer: rating})}
            half={false}
            emptyIcon={<i className='far fa-star'></i>}
            halfIcon={<i className='fa fa-star-half-alt'></i>}
            fullIcon={<i className='fa fa-star'></i>}
            color2={'#ffd700'} />
        );
    } else if (role===constants.ROLE_CONSUMER){
        if(!lend.bookRated){
            reactStars = (
                <ReactStars
                    count={5}
                    size={24}
                    value={lend.calificacionConsumer}
                    onChange={(rating)=>props.changeStateHandler(lend.id, constants.LEND_STATE_DEVUELTO, {bookRate: rating})}
                    half={false}
                    emptyIcon={<i className='far fa-star'></i>}
                    halfIcon={<i className='fa fa-star-half-alt'></i>}
                    fullIcon={<i className='fa fa-star'></i>}
                    color2={'#ffd700'} />
                );
        }else {
            reactStars = <p>LIBRO CALIFICADO</p>
        }
    }
    return reactStars;
};

export default cardDevuelto;