import React from "react";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import * as Util from "../../../util/Utility"
import * as constants from "../../../util/Constants";
import classes from "./CardAceptado.module.css"

const cardAceptado = (props) =>{
    return (
        <Card>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                    Prestamos Aceptados
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="1">
                <Card.Body>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Libro</th>
                            <th>Dias Prestados</th>
                            <th>Precio Dia</th>
                            <th>Consumidor</th>
                            <th>Fecha Prestamo</th>
                            {props.role === constants.ROLE_LENDER ? <th>Acciones</th> : null}
                        </tr>
                        </thead>
                        <tbody>
                        {props.lends.map(lend => {
                            return(
                                <tr key={lend.id}>
                                    <td>{lend.libro}</td>
                                    <td>{lend.cantidadDias}</td>
                                    <td>{lend.precioDia}</td>
                                    <td>{lend.consumidorUsername}</td>
                                    <td>{Util.getFormattedDate(lend.fechaPrestamo)}</td>
                                    {props.role === constants.ROLE_LENDER ?
                                        <td>
                                            <div className={classes.Button}>
                                                <Button
                                                    onClick={() => props.changeStateHandler(lend.id, constants.LEND_STATE_DEVUELTO, null)}
                                                    variant="primary">LIBRO DEVUELTO</Button>
                                            </div>
                                        </td>
                                        :null
                                    }
                            </tr>
                            )
                        })}
                        </tbody>
                    </Table>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
};

export default cardAceptado;