import React from "react";
import Accordion from "react-bootstrap/Accordion";
import * as constants from "../../util/Constants"
import CardSolicitado from "./CardSolicitado/CardSolicitado";
import CardAceptado from "./CardAceptado/CardAceptado";
import CardRechazado from "./CardRechazado/CardRechazado";
import CardDevuelto from "./CardDevuelto/CardDevuelto";
import CardCalificado from "./CardCalificado/CardCalificado";


const prestamosCards = props => {

    let prestamosSolicitados = [];
    let prestamosAceptados = [];
    let prestamosRechazados = [];
    let prestamosDevueltos = [];
    let prestamosCalificados = [];

    const iterableCatalogue = Object.values(props.catalogue);

    for(let lend of iterableCatalogue){
        switch(lend.estado){
            case constants.LEND_STATE_SOLICITADO: prestamosSolicitados.push(lend); break;
            case constants.LEND_STATE_ACEPTADO: prestamosAceptados.push(lend); break;
            case constants.LEND_STATE_RECHAZADO: prestamosRechazados.push(lend); break;
            case constants.LEND_STATE_DEVUELTO: prestamosDevueltos.push(lend); break;
            case constants.LEND_STATE_CALIFICADO: prestamosCalificados.push(lend); break;
            default: break;
        }
    }

    let cardPrestamosSolicitados = null;
    let cardPrestamosAceptados = null;
    let cardPrestamosRechazados = null;
    let cardPrestamosDevueltos = null;
    let cardPrestamosCalificados = null;

    if(prestamosSolicitados.length !== 0){
        cardPrestamosSolicitados = <CardSolicitado role={props.role} changeStateHandler = {props.changeStateHandler} lends={prestamosSolicitados}/>
    }

    if(prestamosAceptados.length !== 0){
        cardPrestamosAceptados = <CardAceptado role={props.role} changeStateHandler = {props.changeStateHandler} lends={prestamosAceptados}/>
    }

   if (prestamosRechazados.length !== 0){
        cardPrestamosRechazados = <CardRechazado role={props.role} lends={prestamosRechazados}/>
    }

    if (prestamosDevueltos.length !== 0){
        cardPrestamosDevueltos = <CardDevuelto role={props.role} changeStateHandler = {props.changeStateHandler} lends={prestamosDevueltos}/>
    }

    if(prestamosCalificados.length !== 0){
        cardPrestamosCalificados = <CardCalificado role={props.role} lends={prestamosCalificados}/>
    }

    return(
        <Accordion defaultActiveKey="0">
            {cardPrestamosSolicitados}
            {cardPrestamosAceptados}
            {cardPrestamosRechazados}
            {cardPrestamosDevueltos}
            {cardPrestamosCalificados}
        </Accordion>
    )
};

export default prestamosCards;