import React from "react";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import * as Util from "../../../util/Utility";

const cardRechazado = (props) =>{
    return (
        <Card>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="2">
                    Prestamos Rechazados
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="2">
                <Card.Body>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Libro</th>
                            <th>Dias Prestados</th>
                            <th>Precio Dia</th>
                            <th>Consumidor</th>
                            <th>Fecha Prestamo</th>
                        </tr>
                        </thead>
                        <tbody>
                        {props.lends.map(lend => {
                            return(
                                <tr key={lend.id}>
                                    <td>{lend.libro}</td>
                                    <td>{lend.cantidadDias}</td>
                                    <td>{lend.precioDia}</td>
                                    <td>{lend.consumidorUsername}</td>
                                    <td>{Util.getFormattedDate(lend.fechaPrestamo)}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </Table>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
};

export default cardRechazado;