import React from "react";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import * as Util from "../../../util/Utility";
import classes from "./CardSolicitado.module.css";
import * as constants from "../../../util/Constants";
import ReactStars from 'react-rating-stars-component';

const cardSolicitado = (props) =>{
    return (
        <Card>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                    Prestamos Solicitados
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="0">
                <Card.Body>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Libro</th>
                            <th>Dias Prestados</th>
                            <th>Precio Dia</th>
                            <th>Consumidor: Username y Calificacion</th>
                            <th>Fecha Prestamo</th>
                            {props.role === constants.ROLE_LENDER ? <th>Acciones</th> : null}
                        </tr>
                        </thead>
                        <tbody>
                        {props.lends.map(lend => {
                            return(
                                <tr key={lend.id}>
                                    <td>{lend.libro}</td>
                                    <td>{lend.cantidadDias}</td>
                                    <td>{lend.precioDia}</td>
                                    <td>
                                        <div className={classes.Inline}>
                                            {lend.consumidorUsername}
                                        </div>
                                        <div className={classes.Inline}>
                                            <ReactStars
                                                count={5}
                                                size={24}
                                                value={lend.calificacionConsumer}
                                                edit={false}
                                                half={false}
                                                emptyIcon={<i className='far fa-star'></i>}
                                                halfIcon={<i className='fa fa-star-half-alt'></i>}
                                                fullIcon={<i className='fa fa-star'></i>}
                                                color2={'#ffd700'} />
                                        </div>
                                    </td>
                                    <td>{Util.getFormattedDate(lend.fechaPrestamo)}</td>
                                    {props.role === constants.ROLE_LENDER ?
                                        <td>
                                            <div className={classes.Inline}>
                                                <Button
                                                    onClick={() => props.changeStateHandler(lend.id, constants.LEND_STATE_ACEPTADO, null)}
                                                    variant="primary">ACEPTAR PRESTAMO</Button>
                                            </div>
                                            <div className={classes.Inline}>
                                                <Button
                                                    onClick={() => props.changeStateHandler(lend.id, constants.LEND_STATE_RECHAZADO, null)}
                                                    variant="danger">RECHAZAR PRESTAMO</Button>
                                            </div>
                                        </td>
                                        :null
                                    }
                                </tr>
                            )
                        })}
                        </tbody>
                    </Table>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
};

export default cardSolicitado;