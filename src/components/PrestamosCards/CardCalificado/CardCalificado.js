import React from "react";
import Card from "react-bootstrap/Card";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Table from "react-bootstrap/Table";
import * as Util from "../../../util/Utility";
import * as constants from "../../../util/Constants"
import ReactStars from 'react-rating-stars-component'

const cardCalificado = (props) =>{



    return (
        <Card>
            <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="4">
                    Prestamos Calificados
                </Accordion.Toggle>
            </Card.Header>
            <Accordion.Collapse eventKey="4">
                <Card.Body>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Libro</th>
                            <th>Dias Prestados</th>
                            <th>Precio Dia</th>
                            <th>Consumidor</th>
                            <th>Fecha Prestamo</th>
                            {props.role===constants.ROLE_CONSUMER ? <th>Calificar Libro</th> : null}
                        </tr>
                        </thead>
                        <tbody>
                        {props.lends.map(lend => {
                            return(
                                <tr key={lend.id}>
                                    <td>{lend.libro}</td>
                                    <td>{lend.cantidadDias}</td>
                                    <td>{lend.precioDia}</td>
                                    <td>{lend.consumidorUsername}</td>
                                    <td>{Util.getFormattedDate(lend.fechaPrestamo)}</td>
                                    {getReactStars(lend, props.role, props)}
                                </tr>
                            )
                        })}
                        </tbody>
                    </Table>
                </Card.Body>
            </Accordion.Collapse>
        </Card>
    )
};


const getReactStars = (lend, role, props) =>{
    let reactStars = null;

    if(role===constants.ROLE_LENDER){
        reactStars = null;
    } else if (role===constants.ROLE_CONSUMER){
        console.log(lend);
        if(!lend.bookRated){
            reactStars = (
                <td>
                <ReactStars
                    count={5}
                    size={24}
                    value={lend.calificacionConsumer}
                    onChange={(rating)=>props.changeStateHandler(lend.id, constants.LEND_STATE_DEVUELTO, {bookRate: rating})}
                    half={false}
                    emptyIcon={<i className='far fa-star'></i>}
                    halfIcon={<i className='fa fa-star-half-alt'></i>}
                    fullIcon={<i className='fa fa-star'></i>}
                    color2={'#ffd700'} />
                </td>
            );
        }else {
            reactStars = <td>LIBRO CALIFICADO</td>
        }
    }
    return reactStars;
};

export default cardCalificado;