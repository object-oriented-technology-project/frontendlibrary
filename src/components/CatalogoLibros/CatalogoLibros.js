import React from "react";
import Card from "react-bootstrap/Card"
import CardColumns from "react-bootstrap/CardColumns";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";

const catalogoLibros = (props) =>{

    return(
        <Container className="p-3">
        <CardColumns>
            {props.catalogue.map(libro => {
                return (
                    <Card className="p-3" key={libro.id}>
                        <Card.Img variant="top" src={libro.portadaImagenUrl} />
                        <Card.Body>
                            <Card.Title>{libro.nombre}</Card.Title>
                            <Card.Text>
                                {libro.autor}
                            </Card.Text>
                            <Button onClick={() => props.onClick(libro.id)} variant="primary">Ver Detalle</Button>
                        </Card.Body>
                    </Card>
                )})}
        </CardColumns>
        </Container>
    );
};

export default catalogoLibros;