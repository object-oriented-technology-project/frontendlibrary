import axios from "axios";

const instance = axios.create(
    {
        baseURL: 'http://localhost:8090',
        withCredentials: true
    }
);

export default instance;