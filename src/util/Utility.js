export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const requiredValidation = (value) =>{
    return value.trim() !== '';
};

export const minLengthValidation = (value, minLength) =>{
    return value.length >= minLength;
};

export const maxLengthValidation = (value, maxLength) =>{
    return value.length <= maxLength;
};

export const emailValidation = (value) =>{
    const regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])";
    return value.match(regex);
};

export const checkValidity = (formElement, rules) =>{
    let isValid = true;

    if(rules.minLength){
        isValid = formElement
    }

    if(rules.maxLength){
        isValid = formElement.value.length <= rules.maxLength && isValid;
    }

    return isValid;
};

export const getFormattedDate = (date) => {
    const actualDate = new Date(date);
    const day = actualDate.getDate();
    const month = actualDate.getMonth() + 1;
    const year = actualDate.getFullYear();
    return day +"/" +month +"/" +year;
};