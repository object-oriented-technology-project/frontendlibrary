export const ROLE_CONSUMER = 'CONSUMER';
export const ROLE_LENDER = 'LENDER';

export const LEND_STATE_SOLICITADO = 'SOLICITADO';
export const LEND_STATE_RECHAZADO = 'RECHAZADO';
export const LEND_STATE_ACEPTADO = 'ACEPTADO';
export const LEND_STATE_DEVUELTO = 'DEVUELTO';
export const LEND_STATE_CALIFICADO = 'CALIFICADO';

export const ROUTE_HOME = "/";

export const ROUTE_PRESTAMOS = "/prestamos";
export const ROUTER_PRESTAMO = "/prestamo";

export const ROUTE_LIBRO = "/libro/:id";
export const ROUTE_LIBRO_NO_ID = "/libro/";
export const ROUTE_LIBRO_ADD = "/addLibro";
export const ROUTE_USUARIO_REGISTER = "/register";
export const ROUTE_USUARIO_DATOS = "/datosUsuario";


export const BUTTON_SUCCESS = "Success";

export const URL_AUTOLOGIN = "/autoLogin";
export const URL_SIGNUP = "/signup";
export const URL_AUTHENTICATE = "/authenticate";
export const URL_LOGOUT = "/logOut";
export const URL_GET_USER_DATA = "/getUserData";
export const URL_CHANGE_USER_DATA = "/changeUserData";
export const URL_DELETE_USER = "/deleteUser";

export const URL_CREATE_LIBRO = "/libros/libro";
export const URL_UPDATE_LIBRO = "/libros/libro";
export const URL_DELETE_LIBRO = "/libros/libro";
export const URL_READ_LIBRO = "/libros/libro";
export const URL_FETCH_LIBROS = "/libros/catalogo";

export const URL_SOLICITAR_PRESTAMO = "/prestamo/solicitar";
export const URL_ACEPTAR_PRESTAMO = "/prestamo/aceptar";
export const URL_RECHAZAR_PRESTAMO = "/prestamo/rechazar";
export const URL_DEVOLVER_PRESTAMO = "/prestamo/devolverLibro";
export const URL_CALIFICAR_CONSUMIDOR = "/prestamo/calificarConsumidor";
export const URL_CALIFICAR_LIBRO = "/prestamo/calificarLibro";
export const URL_FETCH_PRESTAMOS = "/prestamo/catalogo";
