import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import BootstrapInput from "../../../components/UI/BootstrapInput/BootstrapInput"
import {connect} from "react-redux";
import * as actionCreator from "../../../store/actions/index";
import classes from "./Prestamo.module.css"
import Button from "react-bootstrap/Button";
import * as constants from "../../../util/Constants"
import {Redirect} from "react-router";
import * as Util from "../../../util/Utility";

class Prestamo extends Component{
    state={
        controls: {
            bookName: {
                value: "",
                label: "Libro",
                valid: true,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: true,
                    plaintext: false,
                    type: "text"
                },
                validation: {}
            },
            dayPrice: {
                value: "",
                label: "Precio por Dia",
                valid: true,
                touched: false,
                elementType: "input",
                config: {
                    disabled: true,
                    plaintext: false,
                    type: "text"
                },
                validation: {}
            },
            daysQuantity: {
                value: "",
                label: "Cantidad de Dias",
                valid: false,
                touched: false,
                elementType: "input",
                config: {
                    disabled: false,
                    plaintext: false,
                    type: "number"
                },
                validation: {
                    required: true,
                    isInt: true,
                    greaterThan: 0,
                    lesserThan: 31
                }
            }
        }
    };

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls){
            formElementsArray.push(
                {
                    id: key,
                    control: this.state.controls[key]
                }
            );
        }

        if(this.props.bookId == null){
            return null;
        }

        const content = formElementsArray.map(formElement => {
                if(formElement.control.notShown){
                    return null;
                }

                return(
                    <BootstrapInput
                        key={formElement.id}
                        id={formElement.id}
                        elementType={formElement.control.elementType}
                        label={formElement.control.label}
                        value={this.props[formElement.id]}
                        shouldValidate={formElement.control.validation}
                        valid={formElement.control.valid}
                        touched={formElement.control.touched}
                        config={formElement.control.config}
                        onChange = {this.onChangeHandler}/>
                )
            });

        return(
            <div className={classes.Prestamo}>
                {this.props.solicitudeSent ? <Redirect to={constants.ROUTE_PRESTAMOS}/> : null}
                <Form>
                    {content}
                    <Button variant={"primary"} onClick={this.onSolicitudeClickHandler}>SOLICITAR PRESTAMO</Button>
                </Form>
            </div>
        )
    };

    /**
     * Metodo que contrala los cambios a los valores input.
     * 2 way-binding
     * @param formElementId
     * @param event
     */
    onChangeHandler = (formElementId, event) => {
        this.props.onChangeHandler(formElementId, event.target.value)
        const updatedElement = Util.updateObject(this.state.controls[formElementId], {touched: true});
        const updatedControl = Util.updateObject(this.state.controls, {[formElementId]: updatedElement});
        this.setState({controls: updatedControl})
    };

    onSolicitudeClickHandler = () => {
        this.props.onLendSolicitudeHandler(this.props.bookId, this.props.daysQuantity);
    }
}

const mapStateToProps = (state) =>{
    return {
        bookId: state.prestamos.bookId,
        bookName: state.prestamos.bookName,
        dayPrice: state.prestamos.dayPrice,
        daysQuantity: state.prestamos.daysQuantity,
        solicitudeSent: state.prestamos.solicitudeSent
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeHandler: (key, value) => dispatch(actionCreator.onChangeHandler(key, value)),
        onLendSolicitudeHandler: (bookId, daysQuantity) => dispatch(actionCreator.onLendSolicitudeHandler(bookId, daysQuantity))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Prestamo);