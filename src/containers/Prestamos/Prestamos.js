import React, {Component} from "react";
import {connect} from "react-redux"
import * as actionCreator from "../../store/actions/index"
import PrestamosCards from "../../components/PrestamosCards/PrestamosCards"
import Spinner from "../../components/UI/Spinner/Spinner"
import classes from "./Prestamos.module.css"
import Alert from "react-bootstrap/Alert";

class Prestamos extends Component{

    componentDidMount() {
        this.props.fetchCatalogue();
    }

    render() {
        let content = (<PrestamosCards
            role={this.props.role}
            changeStateHandler = {this.changeStateHandler}
            catalogue={this.props.catalogue}/>);

        if(!this.props.loaded){
            content=(<Spinner/>);
        }

        let errors = null;

        if(this.props.error){
            errors = (
                <Alert variant="danger">
                    <p>{this.state.errorMessage}</p>
                </Alert>);
        }

        return(
            <div className={classes.Prestamos}>
                {errors}
                {content}
            </div>
        )
    }

    changeStateHandler = (idLend, newState, extraInfo) =>{
        this.props.changeState(idLend, newState, extraInfo);
    };
}

const mapStateToProps = state => {
    return {
        role: state.auth.role,
        catalogue: state.prestamos.catalogue,
        loading: state.prestamos.loading,
        error: state.prestamos.error,
        loaded: state.prestamos.loaded
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        fetchCatalogue: () => dispatch(actionCreator.onFetchCatalogue()),
        changeState: (idLend, newState, extraInfo) => dispatch(actionCreator.changeLendState(idLend, newState, extraInfo))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Prestamos);