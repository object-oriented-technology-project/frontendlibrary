import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import BootstrapInput from "../../components/UI/BootstrapInput/BootstrapInput";
import * as Util from "../../util/Utility";
import classes from "./Usuario.module.css"
import {connect} from "react-redux";
import Button from "react-bootstrap/Button";
import axios from "../../axios/axios-AuthApp";
import * as actionCreator from "../../store/actions/index"
import * as constants from "../../util/Constants";
import Alert from "react-bootstrap/Alert";

class Usuario extends Component{
    state = {
        loaded: false,
        isEdit: true,
        valid: false,
        errorMessage: null,
        controls: {
            username:{
                value: "",
                label: "Nombre de Usuario",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: false,
                    plaintext: false,
                    type: "text"
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            name:{
                value: "",
                label: "Nombre",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: false,
                    plaintext: false,
                    type: "text"
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            email:{
                value: "",
                label: "Correo Electronico",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: false,
                    plaintext: false,
                    type: "email"
                },
                validation: {
                    required: true,
                    maxLength: 255,
                    isEmail: true
                }
            },
            password:{
                value: "",
                label: "Contrasenia",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: false,
                    plaintext: false,
                    type: "password"
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            passwordCheck:{
                value: "",
                label: "Revisar Contrasenia",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: false,
                    plaintext: false,
                    type: "password"
                },
                validation: {
                    required: true,
                    maxLength: 255,
                    passwordCheck: true
                }
            },
            role:{
                value: "CONSUMER",
                label: "Rol de Usuario",
                valid: true,
                touched: false,
                elementType: 'select',
                options: [
                    {value:'CONSUMER', label:'Consumidor'},
                    {value:'LENDER', label:'Prestador'},
                ],
                config: {
                    disabled: false,
                    plaintext: false
                },
                validation: {
                    required: true
                }
            }
        },
        beforeEditValues: {
            username: null,
            password: null,
            passwordCheck: null,
            name: null,
            email: null,
            role: null
        }
    };

    componentDidMount() {
        if(this.props.role!==null){
            this.loadUserData();
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls){
            formElementsArray.push(
                {
                    id: key,
                    control: this.state.controls[key]
                }
            );
        }

        let errors = null;

        if(this.state.errorMessage !== null){
            errors = (
                <Alert variant="danger">
                    <p>{this.state.errorMessage}</p>
                </Alert>);
        }

        let content = (
                <div className={classes.Usuario}>
                    {errors}
                    <Form>
                        {formElementsArray.map(formElement => {
                            if(formElement.control.notShown){
                                return null;
                            }
                            return(
                                <BootstrapInput
                                    key={formElement.id}
                                    id={formElement.id}
                                    elementType={formElement.control.elementType}
                                    label={formElement.control.label}
                                    value={formElement.control.value}
                                    options={formElement.control.options}
                                    shouldValidate={formElement.control.validation}
                                    valid={formElement.control.valid}
                                    touched={formElement.control.touched}
                                    config={formElement.control.config}
                                    passwordCheck={(formElement.id === "passwordCheck") ? this.state.controls.password.value: null}
                                    onChange = {this.onChangeHandler}/>
                            )})}
                    </Form>
                </div>
        );

        let buttons = null;
        if(this.props.role===null){
            buttons = <Button variant="primary" disabled={!this.state.valid} onClick={this.onRegisterHandler}>Registrar</Button>
        } else if (!this.state.isEdit && this.state.loaded) {
            buttons =
                <React.Fragment>
                    <Button variant="primary" onClick={this.onEditHandler}>Editar</Button>
                    <Button variant="danger" onClick={this.onDeleteHandler}>Eliminar Cuenta</Button>
                </React.Fragment>
        } else if (this.state.isEdit && this.state.loaded){
            buttons = (
                <React.Fragment>
                    <Button variant="primary" disabled={!this.state.valid} onClick={this.onSaveHandler}>Guardar</Button>
                    <Button variant="danger" onClick={this.onEditHandler}>Cancelar</Button>
                </React.Fragment>
            )
        }

        return(
            <React.Fragment>
                {
                    this.state.loaded || this.props.role === null ?
                    <Form>
                        {content}
                        <div className={classes.Button}>
                            {buttons}
                        </div>
                    </Form>
                    : null
                }
            </React.Fragment>
        )
    }

    onSaveHandler = () => {
        const sentData = {
            username: this.state.controls.username.value,
            password: this.state.controls.password.value,
            name: this.state.controls.name.value,
            email: this.state.controls.email.value
        };
        axios.put(constants.URL_CHANGE_USER_DATA, sentData)
            .then(response => {
                this.setState({
                    beforeEditValues: {
                        username: this.state.controls.username.value,
                        password: this.state.controls.password.value,
                        passwordCheck: this.state.controls.password.value,
                        name: this.state.controls.name.value,
                        email: this.state.controls.email.value,
                        role: this.state.controls.role.value
                    }
                });
                this.cancelEdit();
            }).catch(error =>{
                let message = "";
                switch (error.response.data) {
                    case "USERNAME": message = "Ese nombre de usuario ya fue utilizado"; break;
                    case "ROLE": message = "ROL NO VALIDO"; break;
                    case "EMAIL": message = "Ese email ya fue utilizado"; break;
                    default: message="ERROR NO DEFINIDO"; break;
                }
                this.setState({errorMessage: message});
        })
    };

    onDeleteHandler = () => {
        axios.delete(constants.URL_DELETE_USER)
            .then(response =>{
                this.props.onDeleteUser();
            }).catch(error =>{
                console.log(error);
        });
    };

    onRegisterHandler = () =>{
        const sentData ={
            username: this.state.controls.username.value,
            password: this.state.controls.password.value,
            name: this.state.controls.name.value,
            email: this.state.controls.email.value,
            role: this.state.controls.role.value
        };
        axios.post(constants.URL_SIGNUP, sentData)
            .then(response => {
                console.log(response);
                this.props.onAuthSuccess(sentData.username, sentData.name, sentData.role);
            })
            .catch(error => {
                let message = "";
                switch (error.response.data) {
                    case "USERNAME": message = "Ese nombre de usuario ya fue utilizado"; break;
                    case "ROLE": message = "ROL NO VALIDO"; break;
                    case "EMAIL": message = "Ese email ya fue utilizado"; break;
                    default: message="ERROR NO DEFINIDO"; break;
                }
                this.setState({errorMessage: message});
            });
    };

    onChangeHandler = (formElementId, event, isValid) =>{
        const updatedControl = Util.updateObject(this.state.controls[formElementId], {value:event.target.value, touched: true, valid: isValid});
        let updatedControls = Util.updateObject(this.state.controls, {[formElementId]: updatedControl});

        let updatedPasswordCheckControl = null;
        if(formElementId === "password"){
            if(this.state.controls.passwordCheck.value === event.target.value && isValid){
                updatedPasswordCheckControl = Util.updateObject(this.state.controls["passwordCheck"], {valid: true});
            } else {
                updatedPasswordCheckControl = Util.updateObject(this.state.controls["passwordCheck"], {valid: false});
            }
            updatedControls = Util.updateObject(updatedControls, {["passwordCheck"]:updatedPasswordCheckControl});
        }

        let formValid = true;
        for(let formKey in this.state.controls){
            if(!this.state.controls[formKey].config.plaintext) {
                if (formKey === formElementId) {
                    formValid = isValid && formValid;
                } else if (updatedPasswordCheckControl !== null) {
                    formValid = updatedPasswordCheckControl.valid && formValid;
                } else {
                    formValid = this.state.controls[formKey].valid && formValid;
                }
            }
        }

        this.setState({
            controls: updatedControls,
            valid: formValid
        });
    };

    onEditHandler = () =>{
        if(!this.state.isEdit) {
            this.changeStateEditProperties();
        } else {
            this.cancelEdit();
        }
    };

    cancelEdit = () =>{
        let updatedControls = {};
        for (let key in this.state.controls){
            const updatedConfig = Util.updateObject(this.state.controls[key].config, {
                disabled: this.state.isEdit,
                plaintext: this.state.isEdit
            });
            const updatedControl = Util.updateObject(this.state.controls[key], {config: updatedConfig, value: this.state.beforeEditValues[key]});
            updatedControls = Util.updateObject(updatedControls, {[key]: updatedControl});
        }

        //PARA EL PASSWORD CHECK
        const updatedConfigPasswordCheck = Util.updateObject(this.state.controls['passwordCheck'].config, {
            disabled: this.state.isEdit,
            plaintext: this.state.isEdit
        });
        const updatedControlPasswordCheck = Util.updateObject(this.state.controls['passwordCheck'], {config: updatedConfigPasswordCheck, value: this.state.beforeEditValues['password']});
        updatedControls = Util.updateObject(updatedControls, {['passwordCheck']: updatedControlPasswordCheck});
        //SE TERMINA PARA PASSWORD CHECK
        this.setState({isEdit:false, controls: updatedControls});
    };

    loadUserData = () =>{
        let updatedControls = {};
        let updatedBeforeEditValues = {};
        axios.post(constants.URL_GET_USER_DATA)
            .then(response =>{
                for (let key in this.state.controls) {
                    if (key !== 'passwordCheck') {
                        const updatedConfig = Util.updateObject(this.state.controls[key].config, {
                            disabled: this.state.isEdit,
                            plaintext: this.state.isEdit
                        });
                        const updatedControl = Util.updateObject(this.state.controls[key], {
                            config: updatedConfig,
                            value: response.data[key]
                        });
                        updatedBeforeEditValues = Util.updateObject(updatedBeforeEditValues, {[key]: response.data[key]});
                        updatedControls = Util.updateObject(updatedControls, {[key]: updatedControl});
                } else {
                        //PARA EL PASSWORD CHECK
                        const updatedConfigPasswordCheck = Util.updateObject(this.state.controls['passwordCheck'].config, {
                            disabled: this.state.isEdit,
                            plaintext: this.state.isEdit
                        });
                        const updatedControlPasswordCheck = Util.updateObject(this.state.controls['passwordCheck'], {config: updatedConfigPasswordCheck, value: response.data['password']});
                        updatedBeforeEditValues = Util.updateObject(updatedBeforeEditValues, {['passwordCheck']: response.data['password']});
                        updatedControls = Util.updateObject(updatedControls, {['passwordCheck']: updatedControlPasswordCheck});
                        //SE TERMINA PARA PASSWORD CHECK
                    }
                }
                this.setState({isEdit:false, controls: updatedControls, beforeEditValues: updatedBeforeEditValues, loaded: true});
            }).catch(error =>{
        });
    };

    changeStateEditProperties =() =>{
        let updatedControls = {};
        for (let key in this.state.controls){
            if(key !== 'username' && key !== 'role') {
                const updatedConfig = Util.updateObject(this.state.controls[key].config, {
                    disabled: this.state.isEdit,
                    plaintext: this.state.isEdit
                });
                const updatedControl = Util.updateObject(this.state.controls[key], {
                    config: updatedConfig,
                    valid: true,
                    touched: true
                });
                updatedControls = Util.updateObject(updatedControls, {[key]: updatedControl});
            } else {
                const updatedControl = Util.updateObject(this.state.controls[key], {});
                updatedControls = Util.updateObject(updatedControls, {[key]: updatedControl});
            }
        }
        this.setState({isEdit:!this.state.isEdit, controls: updatedControls, valid: true});
    };
}

const mapStateToProps = state =>{
    return{
        role: state.auth.role
    }
};

const mapDispatchToProps = dispatch =>{
    return{
        onAuthSuccess: (username, name, role) => dispatch(actionCreator.authSuccess(username, name, role)),
        onDeleteUser: () => dispatch(actionCreator.logOutSuccess())
    }
};


export default connect(mapStateToProps, mapDispatchToProps) (Usuario);