import React, {Component} from 'react';
import './App.css';
import {connect} from "react-redux";
import { Route, Switch, withRouter } from 'react-router-dom';
import * as constants from "../../util/Constants";
import Layout from "../../hoc/Layout/Layout";
import Libros from "../Libros/Libros";
import * as actionCreator from "../../store/actions/index"
import Prestamos from "../Prestamos/Prestamos";
import Prestamo from "../Prestamos/Prestamo/Prestamo";
import Libro from "../Libros/Libro/Libro";
import {Redirect} from "react-router";
import Usuario from "../Usuario/Usuario";
import AgregarLibro from "../Libros/AgregarLibro/AgregarLibro";

class App extends Component {

    componentDidMount() {
        this.props.onTryAutoSignup();
    }

    render(){
    let routes;

    if(this.props.role === constants.ROLE_CONSUMER){
        routes = (
            <Switch>
                <Route path={constants.ROUTE_HOME} exact component={Libros}/>
                <Route path={constants.ROUTE_PRESTAMOS} exact component={Prestamos}/>
                <Route path={constants.ROUTE_LIBRO} component={Libro}/>
                <Route path={constants.ROUTER_PRESTAMO} component={Prestamo}/>
                <Route path={constants.ROUTE_USUARIO_DATOS} component={Usuario}/>
                <Redirect to="/"/>
            </Switch>
        );
    } else if (this.props.role === constants.ROLE_LENDER) {
        routes = (
            <Switch>
                <Route path={constants.ROUTE_HOME} exact component={Libros}/>
                <Route path={constants.ROUTE_PRESTAMOS} exact component={Prestamos}/>
                <Route path={constants.ROUTE_LIBRO} component={Libro}/>
                <Route path={constants.ROUTE_LIBRO_ADD} component={AgregarLibro}/>
                <Route path={constants.ROUTE_USUARIO_DATOS} component={Usuario}/>
                <Redirect to="/"/>
            </Switch>
        );
    } else {
        routes = (
            <Switch>
                <Route path={constants.ROUTE_HOME} exact component={Libros}/>
                <Route path={constants.ROUTE_LIBRO} component={Libro}/>
                <Route path={constants.ROUTE_USUARIO_REGISTER} component={Usuario}/>
                <Redirect to="/"/>
            </Switch>
        );
    }

    return(
        <div>
          <Layout>
              {routes}
          </Layout>
        </div>
    )
  }
}

const mapStateToProps = state => {
  return{
      role: state.auth.role
  };
};

const mapDispatchToProps = dispatch => {
  return{
      onTryAutoSignup: () => dispatch(actionCreator.autoAuthenticate())
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
