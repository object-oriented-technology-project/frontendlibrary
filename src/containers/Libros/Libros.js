import React, {Component} from "react";
import * as actionCreator from "../../store/actions/index";
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import CatalogoLibros from "../../components/CatalogoLibros/CatalogoLibros";
import * as constants from "../../util/Constants";
import Button from "react-bootstrap/Button";

class Libros extends Component{
    componentDidMount() {
        this.props.fetchCatalogue();
    }

    bookSelectedHandler = (id) =>{
        this.props.history.push(constants.ROUTE_LIBRO_NO_ID +id);
    };

    addBookPage = () => {
        this.props.history.push(constants.ROUTE_LIBRO_ADD);
    };

    render() {
        let content = <Spinner/>;
        let btnAddBook = null;
        if(this.props.catalogue){
            if(this.props.role === constants.ROLE_LENDER){
                btnAddBook = <Button variant="primary" onClick={this.addBookPage}>AGREGAR LIBRO</Button>
            }

            content =
                <React.Fragment>
                    {btnAddBook}
                    <CatalogoLibros
                    onClick={(id) => this.bookSelectedHandler(id)}
                    catalogue={this.props.catalogue}
                    />
                </React.Fragment>
        }

        return(
                <div>
                    {content}
                </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        catalogue: state.libros.catalogue,
        loading: state.libros.load,
        role: state.auth.role
    }
};

const mapDispatchToProps = dispatch => {
    return{
        fetchCatalogue: () => dispatch(actionCreator.initCatalogue(1))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Libros);