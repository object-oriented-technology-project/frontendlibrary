import React, {Component} from "react";
import Spinner from "../../../components/UI/Spinner/Spinner"
import axios from "../../../axios/axios-TodoApp";
import * as constants from "../../../util/Constants";
import classes from "./Libro.module.css"
import Image from "react-bootstrap/Image";
import BootstrapInput from "../../../components/UI/BootstrapInput/BootstrapInput"
import Form from "react-bootstrap/Form";
import * as Util from "../../../util/Utility"
import Button from "react-bootstrap/Button";
import * as actionCreator from "../../../store/actions/index"
import {connect} from "react-redux"
import Alert from "react-bootstrap/Alert";

class Libro extends Component{

    state = {
        controls: {
            id: {
                value:"",
                config: {
                    notShown: true
                },
            },
            autor: {
                value: "",
                label: "Autor",
                valid: true,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: true,
                    plaintext: true,
                    type: "text"
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            nombre: {
                value: "",
                label: "Libro",
                valid: true,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: true,
                    plaintext: true,
                    type: "text"
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            precioDia: {
                value:"",
                label: "Precio por Dia",
                valid: true,
                touched: false,
                elementType: 'input',
                config: {
                    disabled: true,
                    plaintext: true,
                    type: "number"
                },
                validation: {
                    required: true,
                    maxLength: 255,
                    greaterThan: 0,
                    lesserThan: 10
                }
            },
            contraportada: {
                value: "",
                label: "Texto Contraportada",
                valid: true,
                touched: false,
                elementType: 'textarea',
                config: {
                    disabled: true,
                    plaintext: true,
                    rows: 3
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            contadorCalificacion: {
                value:"",
                config: {
                    notShown: true
                },
            },
            sumatoriaCalificacion: {
                value:"",
                config: {
                    notShown: true
                },
            },
            portadaImagenUrl: {
                value: null,
                label: "Imagen de Portada",
                valid: true,
                touched: false,
                elementType: "file",
                config: {
                    justEdit: true,
                    imageFile: null
                },
                validation: {
                    isImage: true
                }
            }
        },
        beforeEditValues: {
            autor: "",
            nombre: "",
            precioDia: "",
            contraportada: "",
            portadaImagenUrl: ""
        },
        loading: false,
        loaded: false,
        isEdit: false,
        valid: true,
        fileAdded: false,
        errorMessage: null
    };

    componentDidMount() {
        const id = this.props.match.params.id; // La ruta es libro/:id
        if(!this.state.loaded){
            this.setState({loading: true});
            axios.get(constants.URL_READ_LIBRO+"/"+id)
                .then(response => {
                    this.setWholeState(response.data);
                    this.setState({loading: false, loaded: true});
                }).catch(error => {
                this.setState({loading: false, loaded: false})
            });
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls){
            formElementsArray.push(
                {
                    id: key,
                    control: this.state.controls[key]
                }
            );
        }

        let errors = null;

        if(this.state.errorMessage !== null){
            errors = (
                <Alert variant="danger">
                    <p>{this.state.errorMessage}</p>
                </Alert>);
        }

        let content = (
            <div className={classes.Libro}>
                {errors}
                <div className={classes.Son}>
                    <Image src={this.state.controls.portadaImagenUrl.value} fluid rounded />
                </div>
                <div className={classes.Son}>
                    <Form>
                    {formElementsArray.map(formElement => {
                        if(formElement.control.config.notShown){
                            return null;
                        }

                        if(formElement.control.config.justEdit && !this.state.isEdit){
                            return null;
                        }
                        return(
                            <BootstrapInput
                                key={formElement.id}
                                id={formElement.id}
                                elementType={formElement.control.elementType}
                                label={formElement.control.label}
                                value={formElement.control.value}
                                shouldValidate={formElement.control.validation}
                                valid={formElement.control.valid}
                                touched={formElement.control.touched}
                                config={formElement.control.config}
                                fileAdded={this.state.fileAdded}
                                onChange = {this.onChangeHandler}
                                onChangeFile = {this.onChangeFileHandler}/>
                        )})}
                    </Form>
                    <div className={classes.Button}>
                        { this.state.isEdit && this.props.role===constants.ROLE_LENDER ?
                            <Button variant="primary" onClick={this.updateHandler} disabled={!this.state.valid}>GUARDAR</Button>
                            :null
                        }
                        {this.props.role === constants.ROLE_LENDER ?
                            <Button variant={this.state.isEdit ? "danger" : "primary"} onClick={this.changeToEditHandler}>{this.state.isEdit ? 'CANCELAR' : 'EDITAR'}</Button>
                            :null
                        }
                        { !this.state.isEdit && this.props.role===constants.ROLE_LENDER ?
                            <Button variant="danger" onClick={this.deleteHandler}>ELIMINAR</Button>
                            :null
                        }
                        {this.props.role === constants.ROLE_CONSUMER ?
                            <Button
                                variant="primary"
                                onClick={this.askForLendHandler}>
                                SOLICITAR PRESTAMO
                            </Button>
                            :null
                        }
                    </div>
                </div>
            </div>
        );

        if (this.props.loading) {
            content = <Spinner/>
        }

        return(
            <div>
                {content}
            </div>
        )
    }

    /**
     * Cuando se cambia de modo "Editar" a modo "Detalle" este metodo realiza los cambios necesarios en
     * el state.
     * @param values
     */
    changeToEditHandler = (values) =>{
        let updatedControls = this.updatedControlsShowlable(this.state.beforeEditValues);
        this.setState({controls: updatedControls, isEdit: !this.state.isEdit, fileAdded: false});
    };

    /**
     * Al recibir la informacion del servidor, este metodo guarda la informacion en el state.
     * @param stateValues
     */
    setWholeState = (stateValues) => {
        let values = {...stateValues};
        let updatedControls = {};
        let updatedBeforeEdit = {};
        for(let key in this.state.controls){
            const updatedControl = Util.updateObject(this.state.controls[key], {value: values[key]});
            updatedControls = Util.updateObject(updatedControls, {[key]: updatedControl});
            updatedBeforeEdit = Util.updateObject(updatedBeforeEdit, {[key]: values[key]});
        }
        this.setState({controls: updatedControls, beforeEditValues: updatedBeforeEdit});
    };

    /**
     * Metodo que contrala los cambios a los valores input.
     * 2 way-binding
     * @param formElementId
     * @param event
     */
    onChangeHandler = (formElementId, event, isValid) => {
        const updatedControl = Util.updateObject(this.state.controls[formElementId], {value:event.target.value, touched: true, valid: isValid});
        const updatedControls = Util.updateObject(this.state.controls, {[formElementId]: updatedControl});

        let formValid = true;
        for(let formKey in this.state.controls){
            if(!this.state.controls[formKey].config.notShown) {
                if (formKey === formElementId) {
                    formValid = isValid && formValid;
                } else {
                    formValid = this.state.controls[formKey].valid && formValid;
                }
            }
        }

        this.setState({
            controls: updatedControls,
            valid: formValid
        });
    };

    onChangeFileHandler = (formElementId, file, isValid) => {
        const updatedControlConfig = Util.updateObject(this.state.controls[formElementId].config, {imageFile: file});
        const updatedControl = Util.updateObject(this.state.controls[formElementId], {config: updatedControlConfig, touched: true, valid: isValid});
        const updatedControls = Util.updateObject(this.state.controls, {[formElementId]: updatedControl});
        this.setState({controls: updatedControls, fileAdded: true});
    };

    /**
     * Metodo que controla el evento de eliminar un libro.
     */
    deleteHandler = () => {
        const sendData = {
            id: this.state.controls.id.value
        };
        axios.delete(constants.URL_DELETE_LIBRO, {data: sendData, errorMessage: null})
            .then(response => {
                this.props.history.replace(constants.ROUTE_HOME);
            }).catch(error => {
                this.setState({errorMessage: "Problema en el servidor"});
        });
    };

    updateHandler = () => {
        let formData = new FormData();
        if(this.state.controls.portadaImagenUrl.touched){
            formData.append("file", this.state.controls.portadaImagenUrl.config.imageFile);
        } else {
            formData.append("file", null);
        }

        formData.append("properties", new Blob([JSON.stringify({
            id: parseInt(this.state.controls.id.value),
            nombre: this.state.controls.nombre.value,
            autor: this.state.controls.autor.value,
            precioDia: this.state.controls.precioDia.value,
            contraportada: this.state.controls.contraportada.value,
        })], {type: "application/json"}));

        let sendData = {
            id: parseInt(this.state.controls.id.value),
            nombre:this.state.controls.nombre.value,
            autor:this.state.controls.autor.value,
            contraportada:this.state.controls.contraportada.value,
            portadaImagenUrl:this.state.controls.portadaImagenUrl.value,
            precioDia: parseInt(this.state.controls.precioDia.value)
        };

        axios({method: 'put', url: constants.URL_UPDATE_LIBRO, data: formData, headers: {'Content-Type': 'undefined' }})
            .then(response => {
                console.log(response.data);
                sendData = Util.updateObject(sendData, {portadaImagenUrl: response.data});
                const updatedControls = this.updatedControlsShowlable(sendData);
                const beforeEditValues = Util.updateObject(sendData, {});
                this.setState({controls: updatedControls, beforeEditValues: beforeEditValues, isEdit: false, fileAdded: false, errorMessage: null});
            }).catch(error =>{
            this.setState({errorMessage: "Problema en el servidor"});
        });
    };

    askForLendHandler = () =>{
        this.props.onLendHandler(this.state.controls.id.value, this.state.controls.nombre.value, this.state.controls.precioDia.value);
        this.props.history.push(constants.ROUTER_PRESTAMO);
    };

    /**
     * Metodo que recibe un objeto con los key:value que se muestran en pantalla.
     * Regresa el objeto con el cual se va a actualizar this.state.controls
     * @param showValues
     * @returns {{}}
     */
    updatedControlsShowlable = (showValues) => {
        const showlableValues = {...showValues};
        let updatedControls = {};
        for(let key in this.state.controls){
            let updatedConfigControl = null;
            let updatedControl = null;

            if (this.state.controls[key].config.notShown){
                updatedConfigControl = Util.updateObject(this.state.controls[key].config, {});
                updatedControl = Util.updateObject(this.state.controls[key], {});
            } else {
                updatedConfigControl = Util.updateObject(this.state.controls[key].config, {
                    disabled: this.state.isEdit,
                    plaintext: this.state.isEdit
                });
                updatedControl = Util.updateObject(this.state.controls[key], {config: updatedConfigControl, value:showlableValues[key]});
            }
            updatedControls = Util.updateObject(updatedControls, {[key]: updatedControl});
        }
        return updatedControls;
    };
}

const mapStateToProps = state => {
    return{
        role: state.auth.role
    }
};

const mapDispatchToProps = dispatch => {
    return{
        onLendHandler: (bookId, bookName, dayPrice) => dispatch(actionCreator.onLendHandler(bookId, bookName, dayPrice))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Libro);