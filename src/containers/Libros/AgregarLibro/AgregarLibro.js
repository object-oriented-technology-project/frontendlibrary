import React, {Component} from "react";
import Form from "react-bootstrap/Form";
import BootstrapInput from "../../../components/UI/BootstrapInput/BootstrapInput";
import * as Util from "../../../util/Utility";
import Button from "react-bootstrap/Button";
import axios from "../../../axios/axios-TodoApp";
import * as constants from "../../../util/Constants";
import classes from "./AgregarLibro.module.css"
import Alert from "react-bootstrap/Alert";

class AgregarLibro extends Component{

    state = {
        valid: false,
        controls: {
            autor: {
                value: "",
                label: "Autor",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    type: "text"
                },
                validation: {
                    required: true,
                    maxLength: 100
                }
            },
            nombre: {
                value: "",
                label: "Libro",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    type: "text"
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            precioDia: {
                value:"",
                label: "Precio por Dia",
                valid: false,
                touched: false,
                elementType: 'input',
                config: {
                    type: "number"
                },
                validation: {
                    required: true,
                    maxLength: 255,
                    greaterThan: 0,
                    lesserThan: 10
                }
            },
            contraportada: {
                value: "",
                label: "Texto Contraportada",
                valid: false,
                touched: false,
                elementType: 'textarea',
                config: {
                    rows: 3
                },
                validation: {
                    required: true,
                    maxLength: 255
                }
            },
            portadaImagenUrl: {
                value: null,
                label: "Imagen de Portada",
                valid: true,
                touched: false,
                elementType: "file",
                config: {
                    imageFile: null
                },
                validation: {
                    isImage: true
                }
            }
        },
        loading: false,
        fileAdded: false,
        errorMessage: null
    };

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls){
            formElementsArray.push(
                {
                    id: key,
                    control: this.state.controls[key]
                }
            );
        }

        let errors = null;

        if(this.state.errorMessage !== null){
            errors = (
                <Alert variant="danger">
                    <p>{this.state.errorMessage}</p>
                </Alert>);
        }

        return(
            <div className={classes.AgregarLibro}>
                {errors}
                <Form>
                    {formElementsArray.map(formElement => {
                        if(formElement.control.config.notShown){
                            return null;
                        }
                        return(
                            <BootstrapInput
                                key={formElement.id}
                                id={formElement.id}
                                elementType={formElement.control.elementType}
                                label={formElement.control.label}
                                value={formElement.control.value}
                                shouldValidate={formElement.control.validation}
                                valid={formElement.control.valid}
                                touched={formElement.control.touched}
                                config={formElement.control.config}
                                fileAdded={this.state.fileAdded}
                                onChange = {this.onChangeHandler}
                                fileAdded={this.state.fileAdded}
                                onChangeFile = {this.onChangeFileHandler}/>
                        )})}
                    <Button onClick={this.onSaveHandler} disabled={!this.state.valid}>Guardar</Button>
                </Form>
            </div>
        )
    }

    onSaveHandler = () =>{
        let formData = new FormData();
        formData.append("file", this.state.controls.portadaImagenUrl.value);
        formData.append("properties", new Blob([JSON.stringify({
            nombre: this.state.controls.nombre.value,
            autor: this.state.controls.autor.value,
            precioDia: this.state.controls.precioDia.value,
            contraportada: this.state.controls.contraportada.value,
        })], {type: "application/json"}));
        axios({method: 'post', url: constants.URL_CREATE_LIBRO, data: formData, headers: {'Content-Type': 'undefined' }})
            .then(res => {
                this.props.history.push(constants.ROUTE_HOME);
            }).catch(error => {
            this.setState({errorMessage: "Problema en el servidor"});
        });
    };

    onChangeHandler = (formElementId, event, isValid) => {
        const updatedControl = Util.updateObject(this.state.controls[formElementId], {value:event.target.value, touched: true, valid: isValid});
        const updatedControls = Util.updateObject(this.state.controls, {[formElementId]: updatedControl});

        let formValid = true;
        for(let formKey in this.state.controls){
            if(!this.state.controls[formKey].config.plaintext) {
                if (formKey === formElementId) {
                    formValid = isValid && formValid;
                } else {
                    formValid = this.state.controls[formKey].valid && formValid;
                }
            }
        }

        this.setState({
            controls: updatedControls,
            valid: formValid
        });
    };

    onChangeFileHandler = (formElementId, file, isValid) => {
        const updatedControlConfig = Util.updateObject(this.state.controls[formElementId].config, {imageFile: file});
        const updatedControl = Util.updateObject(this.state.controls[formElementId], {config: updatedControlConfig, touched: true, valid: isValid});
        const updatedControls = Util.updateObject(this.state.controls, {[formElementId]: updatedControl});
        this.setState({controls: updatedControls, fileAdded: true});
    };
}

export default AgregarLibro;