import * as actionTypes from '../actions/actionTypes';
import * as Util from "../../util/Utility";

const initialState = {
    catalogue: [],
    loading: false,
    error: false
};

const reducer = (state=initialState, action) =>{
    switch (action.type) {
        case actionTypes.CATALOGUE_FETCH_START: return catalogueFetchStart(state, action);
        case actionTypes.CATALOGUE_FETCH_SUCCESS: return catalogueFetchSuccess(state, action);
        case actionTypes.CATALOGUE_FETCH_FAIL: return catalogueFetchFail(state, action);
        case actionTypes.BOOK_DETAIL_START: return bookDetailStart(state, action);
        case actionTypes.BOOK_DETAIL_FAIL: return bookDetailFail(state, action);
        default: return state;
    }
};

const catalogueFetchStart =  (state, action) => {
    return Util.updateObject(state, {loading: true, error: false});
};

const catalogueFetchSuccess =  (state, action) => {
    return Util.updateObject(state, {catalogue:action.catalogue, loading: false, error: false});
};

const catalogueFetchFail =  (state, action) => {
    return Util.updateObject(state, {loading: false, error: true})
};

const bookDetailStart = (state, action) => {
    return Util.updateObject(state, {loading: true, error: false});
};

const bookDetailFail = (state, action) => {
    return Util.updateObject(state, {loading: false, error:true});
};

export default reducer;