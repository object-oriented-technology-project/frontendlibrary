import * as actionTypes from "../actions/actionTypes";
import * as Util from "../../util/Utility";

const initialState ={
    catalogue: null,
    loading: false,
    error: false,
    loaded: false,

//DATOS CARGADOS DE LA PANTALLA LIBRO A LA PANTALLA PRESTAMO
    bookId: null,
    bookName: null,
    dayPrice: null,
    daysQuantity: null,
    solicitudeSent: false
};

const reducer = (state=initialState, action) =>{
    switch(action.type) {
        case (actionTypes.SOLICITUDE_INIT): return solicitudeInit(state, action);
        case (actionTypes.SOLICITUDE_CHANGE): return solicitudeChange(state, action);
        case (actionTypes.SOLICITUDE_LEND_START): return solicitudeLendStart(state, action);
        case (actionTypes.SOLICITUDE_LEND_SUCCESS): return solicitudeLendSuccess(state, action);
        case (actionTypes.SOLICITUDE_LEND_FAIL): return solicitudeLendFail(state, action);
        case (actionTypes.CATALOGUE_FETCH_START): return catalogueFetchStart (state, action);
        case (actionTypes.CATALOGUE_FETCH_SUCCESS): return catalogueFetchSuccess (state, action);
        case (actionTypes.CATALOGUE_FETCH_FAIL): return catalogueFetchFail (state, action);
        case (actionTypes.LEND_CHANGE_START): return lendChangeStart(state, action);
        case (actionTypes.LEND_CHANGE_SUCCESS): return lendChangeSuccess(state, action);
        case (actionTypes.LEND_CHANGE_FAIL): return lendChangeFail(state, action);
        case (actionTypes.LEND_CHANGE_STATE_HELPER): return changeStateHelper(state, action);
        case (actionTypes.BOOK_RATED_HANDLER): return bookRatedHandler(state, action);
        case (actionTypes.BOOK_RATED_SUCCESFUL): return bookRatedSuccesful(state, action);
        case (actionTypes.BOOK_RATED_FAIL): return bookRatedFail(state, action);
        default: return state;
    }
};

const solicitudeInit = (state, action) => {
    return Util.updateObject(state, {
        bookId: action.bookId,
        bookName: action.bookName,
        dayPrice: action.dayPrice,
        daysQuantity: action.daysQuantity,
        solicitudeSent: false
    })
};

const solicitudeChange = (state, action) =>{
    return Util.updateObject(state, {
        [action.key]:action.value,
        touched: true
    });
};

const solicitudeLendStart = (state, action) =>{
    return Util.updateObject(state, {loading: true});
};

const solicitudeLendSuccess = (state, action) =>{
    return Util.updateObject(state, {
        bookId: null,
        bookName: null,
        dayPrice: null,
        solicitudeSent: true,
        error: false,
        loading: false});
};

const solicitudeLendFail = (state, action) =>{
    return Util.updateObject(state, {solicitudeSent: false, error: true, loading: false});
};

const catalogueFetchStart = (state, action) => {
    return Util.updateObject(state, {loading: true});
};

const catalogueFetchSuccess = (state, action) => {
    return Util.updateObject(state, {
        catalogue: action.catalogue,
        loading: false,
        loaded: true
    });
};

const catalogueFetchFail = (state, action) => {
    return Util.updateObject(state, {error: true, loading: false});
};

const lendChangeStart = (state, action) =>{
    return Util.updateObject(state, {loading: true});
};

const lendChangeSuccess = (state, action) =>{
    return Util.updateObject(state, {
        loading: false,
        loaded: true
    });
};

const lendChangeFail = (state, action) =>{
    return Util.updateObject(state, {error: true, loading: false});
};

const changeStateHelper = (state, action) => {
    const iterableCatalogue = Object.values(state.catalogue);
    let updatedLends = [];
    for(let index in iterableCatalogue){
        const lend = iterableCatalogue[index];
        let updatedLend = Util.updateObject(lend, {});
        if(lend.id === action.id){
            updatedLend = Util.updateObject(lend, {estado: action.newState});
        }
        updatedLends.push(updatedLend);
    }
    const updatedState = Util.updateObject(state, {catalogue: updatedLends});
    return updatedState;
};

const bookRatedHandler = (state, action) =>{
    const iterableCatalogue = Object.values(state.catalogue);
    let updatedLends = [];
    for(let index in iterableCatalogue){
        const lend = iterableCatalogue[index];
        let updatedLend = Util.updateObject(lend, {});
        if(lend.id === action.id){
            updatedLend = Util.updateObject(lend, {bookRated: true});
        }
        updatedLends.push(updatedLend);
    }
    const updatedState = Util.updateObject(state, {catalogue: updatedLends});
    return updatedState;
};

const bookRatedSuccesful = (state, action) => {
    return Util.updateObject(state, {
            loading: false,
            error: false
        })
};

const bookRatedFail = (state, action) =>{
    return Util.updateObject(state, {
        loading: false,
        error: true
    })
};

export default reducer;