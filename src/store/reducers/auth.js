import * as actionTypes from '../actions/actionTypes';
import * as Util from "../../util/Utility";

const initialState = {
    name: null,
    username: null,
    role: null,
    error: false,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.AUTH_START: return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_AUTOLOGIN_FAIL: return authAutoFail(state, action);
        case actionTypes.AUTH_LOGOUT_START: return authLogoutStart(state, action);
        case actionTypes.AUTH_LOGOUT_SUCCESS: return authLogoutSuccess(state, action);
        case actionTypes.AUTH_LOGOUT_FAIL: return authLogoutFail(state, action);
        default: return state;
    }
};

const authStart = (state, action) =>{
  return Util.updateObject(state, {error: false, loading: true})
};

const authSuccess = (state, action) => {
    return Util.updateObject(state, {
        name: action.name,
        role: action.role,
        username: action.username,
        error: false
    });
};

const authFail = (state, action) =>{
    return Util.updateObject(state, {
        name: null,
        username: null,
        role: null,
        error: true
    });
};

const authAutoFail = (state, action) =>{
    return Util.updateObject(state, {
        name: null,
        username: null,
        role: null,
        error: false
    });
};

const authLogoutStart = (state, action) =>{
    return Util.updateObject(state, {
        loading: true,
        error: false
    });
};

const authLogoutSuccess = (state, action) =>{
    return Util.updateObject(state, {
        name: null,
        username: null,
        role: null,
        error: false,
        loading: false
    });
};

const authLogoutFail = (state, action) =>{
    return Util.updateObject(state, {
        loading: false,
        error: true
    });
};

export default reducer;