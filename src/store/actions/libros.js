import axios from "../../axios/axios-TodoApp";
import * as actionTypes from "./actionTypes";
import * as constants from "../../util/Constants"

export const initCatalogue = (page) =>{
    return dispatch => {
        dispatch(fetchCatalogueStart());
        const fetchData = {
            pagina: 1
        };
        axios.post(constants.URL_FETCH_LIBROS, fetchData)
            .then(response => {
                const catalogue = response.data;
                dispatch(fetchCatalogueSuccess(catalogue))
            }).catch(error=>{
                dispatch(fetchCatalogueFail());
        });
    }
};

//------------------------- NON IMPORTED ------------------------------------------------------------------------

const fetchCatalogueStart = () =>{
    return{
        type: actionTypes.CATALOGUE_FETCH_START
    }
};

const fetchCatalogueSuccess = (catalogue) =>{
    return{
        type: actionTypes.CATALOGUE_FETCH_SUCCESS,
        catalogue: catalogue
    }
};

const fetchCatalogueFail = () =>{
    return{
        type: actionTypes.CATALOGUE_FETCH_FAIL
    }
};
