import axios from "../../axios/axios-AuthApp"
import * as actionTypes from "./actionTypes"
import * as constants from "../../util/Constants"

export const auth = (username, password) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            username: username,
            password: password
        };
        axios.post(constants.URL_AUTHENTICATE, authData)
            .then(response => {
                let username = response.data.username;
                let name = response.data.name;
                let role = response.data.role;
                dispatch(authSuccess(username, name, role));
            }).catch(error =>{
                dispatch(authFail());
        })
    }
};

export const autoAuthenticate = () =>{
    return dispatch => {
        dispatch(authStart());
        axios.post(constants.URL_AUTOLOGIN)
            .then(response => {
                let username = response.data.username;
                let name = response.data.name;
                let role = response.data.role;
                dispatch(authSuccess(username, name, role));
            }).catch(error => {
            dispatch(authAutoLoginFail());
        })
    }
};

export const authSuccess = (username, name, role) =>{
    return {
        type: actionTypes.AUTH_SUCCESS,
        name: name,
        role: role
    }
};

export const onLogout = () =>{
    return dispatch =>{
        dispatch(logOutStart());
        axios.post(constants.URL_LOGOUT)
            .then(response => {
                dispatch(logOutSuccess());
            }).catch(error =>{
                dispatch(logOutFail());
        })
    }
};

export const logOutSuccess = () =>{
    return{
        type: actionTypes.AUTH_LOGOUT_SUCCESS
    }
};

//---------------------------- NON-EXPORTED-----------------------------------------------------------------------------

const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
};

const authFail = () =>{
    return {
        type: actionTypes.AUTH_FAIL
    }
};

const authAutoLoginFail = () =>{
  return{
      type: actionTypes.AUTH_AUTOLOGIN_FAIL
  }
};

const logOutStart = () =>{
    return{
        type: actionTypes.AUTH_LOGOUT_START
    }
};

const logOutFail = () =>{
    return{
        type: actionTypes.AUTH_LOGOUT_FAIL
    }
};