import * as actionTypes from "./actionTypes";
import axios from "../../axios/axios-TodoApp";
import * as constants from "../../util/Constants"

export const onLendHandler = (bookId, bookName, dayPrice) =>{
    return {
        type: actionTypes.SOLICITUDE_INIT,
        bookId: bookId,
        bookName: bookName,
        dayPrice: dayPrice,
        daysQuantity: 0
    };
};

export const onChangeHandler = (key, value) => {
    return {
        type: actionTypes.SOLICITUDE_CHANGE,
        key: key,
        value: value
    }
};

export const onLendSolicitudeHandler = (bookId, daysQuantity) =>{
    return dispatch => {
        dispatch(lendSolicitudeStart());
        const dataSent = {
            libroId: parseInt(bookId),
            cantidadDias: parseInt(daysQuantity)
        };
        axios.post(constants.URL_SOLICITAR_PRESTAMO, dataSent)
            .then(response =>{
                dispatch(lendSolicitudeSuccess());
            }).catch(error =>{
                dispatch(lendSolicitudeFail());
        });
    }
};

export const onFetchCatalogue = () =>{
    return dispatch => {
        dispatch(fetchCatalogueStart());
        axios.get(constants.URL_FETCH_PRESTAMOS)
            .then(response => {
                dispatch(fetchCatalogueSuccess(response.data));
            }).catch(error =>{
                dispatch(fetchCatalogueFail());
        })
    }
};

export const changeLendState = (idLend, newState, extraInfo) =>{
    return dispatch =>{
        dispatch(lendChangeStateStart());
            switch (newState) {
                case constants.LEND_STATE_ACEPTADO:
                    acceptLend(dispatch, idLend);
                    break;
                case constants.LEND_STATE_RECHAZADO:
                    rejectLend(dispatch, idLend);
                    break;
                case constants.LEND_STATE_DEVUELTO:
                    if(extraInfo) {
                        if (extraInfo.bookRate) {
                            bookRate(dispatch, idLend, extraInfo);
                        }
                    }else {
                        returnLend(dispatch, idLend);
                    }
                    break;
                case constants.LEND_STATE_CALIFICADO:
                    if(extraInfo.bookRate){
                        bookRate(dispatch, idLend, extraInfo);
                    } else {
                        rateConsumer(dispatch, idLend, extraInfo);
                    }
                    break;
            }
        }
};

//----------------------------------NON EXPORTED METHODS --------------------------------------------------------------

const lendSolicitudeStart = () =>{
    return {
        type: actionTypes.SOLICITUDE_LEND_START
    }};

const lendSolicitudeSuccess = () =>{
    return {
        type: actionTypes.SOLICITUDE_LEND_SUCCESS
    }
};

const lendSolicitudeFail = () =>{
    return {
        type: actionTypes.SOLICITUDE_LEND_FAIL
    }};

const fetchCatalogueStart = () =>{
    return {
        type: actionTypes.CATALOGUE_FETCH_START
    }
};

const fetchCatalogueSuccess = (data) =>{
    return {
        type: actionTypes.CATALOGUE_FETCH_SUCCESS,
        catalogue: data
    }
};

const fetchCatalogueFail = () =>{
    return {
        type: actionTypes.CATALOGUE_FETCH_FAIL
    }
};

const lendChangeStateStart = () =>{
    return {
        type: actionTypes.LEND_CHANGE_START
    }
};

const lendChangeStateSuccess = () =>{
    return {
        type: actionTypes.LEND_CHANGE_SUCCESS
    }
};

const lendChangeStateFail = () =>{
    return {
        type: actionTypes.LEND_CHANGE_FAIL
    }
};

const changeStateHelper = (id, newState) => {
    return{
        type: actionTypes.LEND_CHANGE_STATE_HELPER,
        id: id,
        newState: newState
    }
};

const acceptLend = (dispatch, id) =>{
        const sentData = {id: id};
        axios.put(constants.URL_ACEPTAR_PRESTAMO, sentData)
            .then(response => {
                dispatch(changeStateHelper(id, constants.LEND_STATE_ACEPTADO))
                dispatch(lendChangeStateSuccess());
            }).catch(error => {
                dispatch(lendChangeStateFail());
        });
};

const rejectLend = (dispatch, id) =>{
        const sentData = {id: id};
        axios.put(constants.URL_RECHAZAR_PRESTAMO, sentData)
            .then(response => {
                dispatch(changeStateHelper(id, constants.LEND_STATE_RECHAZADO));
                dispatch(lendChangeStateSuccess());
            }).catch(error => {
            dispatch(lendChangeStateFail());
        });
};

const returnLend = (dispatch, id) =>{
        const sentData = {id: id};
        axios.put(constants.URL_DEVOLVER_PRESTAMO, sentData)
            .then(response => {
                dispatch(changeStateHelper(id, constants.LEND_STATE_DEVUELTO));
                dispatch(lendChangeStateSuccess());
            }).catch(error => {
            dispatch(lendChangeStateFail());
        });
};

const rateConsumer = (dispatch, id, extraInfo) =>{
    const sentData = {
        id: id,
        calificacionConsumidor: extraInfo.ratingConsumer,
    };
    axios.put(constants.URL_CALIFICAR_CONSUMIDOR, sentData)
        .then(response => {
            dispatch(changeStateHelper(id, constants.LEND_STATE_CALIFICADO));
            dispatch(lendChangeStateSuccess());
        }).catch(error => {
        dispatch(lendChangeStateFail());
    });
};

const bookRate = (dispatch, id, extraInfo) =>{
  const sentData = {
      id: id,
      calificacionLibro: extraInfo.bookRate
  };

  axios.put(constants.URL_CALIFICAR_LIBRO, sentData)
      .then(response =>{
          dispatch(bookRatedHandler(id));
          dispatch(bookRatedSuccesful());
      }).catch(error =>{
          dispatch(bookRatedError())
  })
};

const bookRatedHandler = (id) =>{
    return{
        type: actionTypes.BOOK_RATED_HANDLER,
        id: id
    }
};

const bookRatedSuccesful = () =>{
    return{
        type: actionTypes.BOOK_RATED_SUCCESFUL
    }
};

const bookRatedError = () =>{
    return{
        type:actionTypes.BOOK_RATED_FAIL
    }
};